﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Machine.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Chip8
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.IO;

    using Emulator.Core;

    #endregion

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public partial class Machine : MachineBase
    {
        #region Constants

        /// <summary>
        ///     The font set.
        /// </summary>
        private const string FontSet = "FontSet";

        /// <summary>
        ///     The gfx.
        /// </summary>
        private const string Gfx = "gfx";

        /// <summary>
        ///     The interpreter.
        /// </summary>
        private const string Interpreter = "Interpreter";

        /// <summary>
        ///     The pc increment.
        /// </summary>
        private const int PcIncrement = 2;

        /// <summary>
        ///     The rom.
        /// </summary>
        private const string Rom = "Rom";

        #endregion

        #region Fields

        /// <summary>
        ///     The delay_timer.
        /// </summary>
        private byte _delayTimer;

        /// <summary>
        ///     The _is initialized.
        /// </summary>
        private bool _isInitialized;

        /// <summary>
        ///     The _key definition.
        /// </summary>
        private List<KeyMap> _keyDefinition;

        /// <summary>
        ///     The key.
        /// </summary>
        private bool[] _keyStates = new bool[16];

        /// <summary>
        ///     The sound_timer.
        /// </summary>
        private byte _soundTimer;

        /// <summary>
        ///     The _sp.
        /// </summary>
        private byte _sp = 0;

        /// <summary>
        ///     The _stack.
        /// </summary>
        private byte[] _stack = new byte[16];

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Machine" /> class.
        /// </summary>
        public Machine()
            : base(4096, 16, 16)
        {
            this.Initialize();
            this.Vf = this.V[15];
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The update screen.
        /// </summary>
        public event EventHandler UpdateScreen;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether draw flag.
        /// </summary>
        public bool DrawFlag { get; set; }

        /// <summary>
        ///     The _is running.
        /// </summary>
        public bool IsRunning { get; set; }

        /// <summary>
        ///     Gets the vf.
        /// </summary>
        public byte Vf { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the pressed key.
        /// </summary>
        private byte PressedKey { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The load image.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public bool LoadImage(string path)
        {
            try
            {
                var fileStream = new FileStream(path, FileMode.Open);
                var reader = new BinaryReader(fileStream);
                var readBytes = reader.ReadBytes((int)fileStream.Length);
                for (var index = 0; index < (int)fileStream.Length; index++)
                {
                    var @byte = readBytes[index];
                    this.Memory[Rom, index] = @byte;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// The run emulation.
        /// </summary>
        /// <param name="updateAction">
        /// The update Action.
        /// </param>
        public void RunEmulation(Action updateAction)
        {
            while (true)
            {
                while (this._isInitialized && this.IsRunning)
                {
                    this.RunCycle();
                    if (this.DrawFlag) this.DrawGraphics();
                }
                
                if (updateAction != null) updateAction.Invoke();
            }
        }

        /// <summary>
        /// The set key press.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        public void SetKeyPress(char key)
        {
            var keyMap = this._keyDefinition.Find(map => map.Character == key);
            keyMap.Press();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on update screen.
        /// </summary>
        protected virtual void OnUpdateScreen()
        {
            var handler = this.UpdateScreen;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        /// <summary>
        ///     The clear display.
        /// </summary>
        private void ClearDisplay()
        {
            this.Peripherals[Gfx].Memory.ClearMemory();
        }

        /// <summary>
        ///     The clear memory.
        /// </summary>
        private void ClearMemory()
        {
            this.Memory.ClearMemory();
        }

        /// <summary>
        ///     The clear registers.
        /// </summary>
        private void ClearRegisters()
        {
            Array.Clear(this.V, 0, this.V.Length - 1);
        }

        /// <summary>
        ///     The clear stack.
        /// </summary>
        private void ClearStack()
        {
            Array.Clear(this.Stack, 0, this.Stack.Length - 1);
        }

        /// <summary>
        ///     The draw graphics.
        /// </summary>
        private void DrawGraphics()
        {
            this.DrawFlag = false;
            this.OnUpdateScreen();
        }

        /// <summary>
        ///     The initialize.
        /// </summary>
        private void Initialize()
        {
            this.SetUpPeripherals();
            this.SetUpMemory();
            this.InitializeOpCodes();

            //// this.SetupGraphics();
            this.SetupInput();

            this.SetDefaults();

            this.ClearDisplay();
            this.ClearStack();
            this.ClearRegisters();
            this.ClearMemory();
            this.LoadFontSet();

            this._isInitialized = true;
        }

        /// <summary>
        ///     The run cycle.
        /// </summary>
        private void RunCycle()
        {
            // Fetch Opcode
            var code = this.Memory.GetMemoryAsWord(this.PC).ToString("X4");

            // Decode Opcode
            this.OpCode = this.OpCodes.FindWithCode(code);

            this.OpCode.Value = code;

            // Execute Opcode
            if (this.OpCode != null) this.OpCode.ExecuteCommand();

            // Update timers
            if (this._delayTimer > 0) --this._delayTimer;

            if (this._soundTimer <= 0) return;

            if (this._soundTimer == 1) Console.Beep();

            --this._soundTimer;
        }

        /// <summary>
        ///     The set defaults.
        /// </summary>
        private void SetDefaults()
        {
            this.PC = (ushort)"0x200".ToInt();
            this.OpCode = null;
            this.I = 0;
            this.SP = 0;
        }

        /// <summary>
        ///     The set up memory.
        /// </summary>
        private void SetUpMemory()
        {
            this.Memory.MemoryMap.Add(Interpreter, new Range { Begin = 0, End = 511 });
            this.Memory.MemoryMap.Add(FontSet, new Range { Begin = 80, End = 160 });
            this.Memory.MemoryMap.Add(Rom, new Range { Begin = 512, End = 4095 });
        }

        /// <summary>
        ///     The set up peripherals.
        /// </summary>
        private void SetUpPeripherals()
        {
            this.Peripherals.Add(Gfx, new PeripheralBase(64 * 32));
        }

        #endregion
    }
}