﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeyMap.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Chip8
{
    #region Usings

    using System;

    #endregion

    /// <summary>
    ///     The key map.
    /// </summary>
    public class KeyMap
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyMap"/> class.
        /// </summary>
        /// <param name="character">
        /// The character.
        /// </param>
        /// <param name="keyValue">
        /// The key value.
        /// </param>
        public KeyMap(char character, int keyValue)
        {
            this.Character = character;
            this.IsPressed = false;
            this.KeyValue = keyValue;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The key pressed.
        /// </summary>
        public event EventHandler<EventArgs> KeyPressed;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the character.
        /// </summary>
        public char Character { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is pressed.
        /// </summary>
        public bool IsPressed { get; set; }

        /// <summary>
        ///     Gets or sets the key value.
        /// </summary>
        public int KeyValue { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The press.
        /// </summary>
        public void Press()
        {
            this.IsPressed = true;
            this.OnKeyPressed();
        }

        /// <summary>
        ///     The release.
        /// </summary>
        public void Release()
        {
            this.IsPressed = false;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on key pressed.
        /// </summary>
        private void OnKeyPressed()
        {
            var temp = this.KeyPressed;
            if (temp != null)
            {
                temp(this, new EventArgs());
            }
        }

        #endregion
    }
}