﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Machine.Definitions.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Chip8
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Xml.Schema;

    using Emulator.Core;

    #endregion

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public partial class Machine
    {
        #region Methods

        /// <summary>
        ///     The initialize op codes.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1118:ParameterMustNotSpanMultipleLines", Justification = "Reviewed. Suppression is OK here."),SuppressMessage("StyleCop.CSharp.SpacingRules", "SA1003:SymbolsMustBeSpacedCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        private void InitializeOpCodes()
        {
            // ReSharper disable StringLiteralTypo
            this.OpCodes.Add(
                new OpCode(
                    "0NNN",
                    "Calls RCA 1802 program at address NNN.",
                    value =>
                        {
                            Stack[SP] = PC;
                            ++SP;
                            PC = value.ConvertNNN();
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "00E0",
                    "Clears the screen.",
                    () =>
                        {
                            Peripherals[Gfx].Memory.ClearMemory();
                            DrawFlag = true;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "00EE",
                    "Returns from a subroutine.",
                    () =>
                        {
                            --SP;
                            PC = Stack[SP];
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "1NNN",
                    "Jumps to address NNN.",
                    value =>
                        {
                            PC = value.ConvertNNN();
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "2NNN",
                    "Calls subroutine at NNN.",
                    value =>
                        {
                            Stack[SP] = PC;
                            ++SP;
                            PC = value.ConvertNNN();
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "3XNN",
                    "Skips the next instruction if VX equals NN.",
                    value =>
                        {
                            if (V[value.ConvertX()] == value.ConvertNN()) PC += PcIncrement * 2;
                            else PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "4XNN",
                    "Skips the next instruction if VX doesn't equal NN.",
                    value =>
                        {
                            if (V[value.ConvertX()] != value.ConvertNN()) PC += PcIncrement * 2;
                            else PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "5XY0",
                    "Skips the next instruction if VX equals VY.",
                    value =>
                        {
                            if (V[value.ConvertX()] == value.ConvertY()) PC += PcIncrement * 2;
                            else PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "6XNN",
                    "Sets VX to NN.",
                    value =>
                        {
                            V[value.ConvertX()] = (byte)value.ConvertNN();
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "7XNN",
                    "Adds NN to VX.",
                    value =>
                        {
                            V[value.ConvertX()] += (byte)value.ConvertNN();
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "8XY0",
                    "Sets VX to the value of VY.",
                    value =>
                        {
                            V[value.ConvertX()] = V[value.ConvertY()];
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "8XY1",
                    "Sets VX to VX or VY.",
                    value =>
                        {
                            V[value.ConvertX()] |= V[value.ConvertY()];
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "8XY2",
                    "Sets VX to VX and VY.",
                    value =>
                        {
                            V[value.ConvertX()] &= V[value.ConvertY()];
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "8XY3",
                    "Sets VX to VX xor VY.",
                    value =>
                        {
                            V[value.ConvertX()] ^= V[value.ConvertY()];
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "8XY4",
                    "Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.",
                    value =>
                        {
                            var valueX = V[value.ConvertX()];
                            var valueY = V[value.ConvertY()];
                            this.Vf = (byte)(valueY > valueX ? 1 : 0);

                            V[value.ConvertX()] += valueY;
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "8XY5",
                    "VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.",
                    value =>
                        {
                            var valueX = V[value.ConvertX()];
                            var valueY = V[value.ConvertY()];
                            this.Vf = (byte)(valueY < valueX ? 0 : 1);

                            V[value.ConvertX()] -= valueY;
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "8XY6",
                    "Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift.",
                    value =>
                        {
                            var valueX = V[value.ConvertX()];
                            this.Vf = (byte)(valueX & 1);
                            V[value.ConvertX()] = (byte)(valueX >> 1);
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "8XY7",
                    "Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.",
                    value =>
                        {
                            var valueX = V[value.ConvertX()];
                            var valueY = V[value.ConvertY()];
                            this.Vf = (byte)(valueX < valueY ? 0 : 1);

                            V[value.ConvertX()] = (byte)(valueY - valueX);
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "8XYE",
                    "Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift.",
                    value =>
                        {
                            var valueX = V[value.ConvertX()];
                            this.Vf = (byte)(valueX & 4096);
                            V[value.ConvertX()] = (byte)(valueX << 1);
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "9XY0",
                    "Skips the next instruction if VX doesn't equal VY.",
                    value =>
                        {
                            if (V[value.ConvertX()] != V[value.ConvertY()]) PC += PcIncrement * 2;
                            else PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "ANNN",
                    "Sets I to the address NNN.",
                    value =>
                        {
                            I = value.ConvertNNN();
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "BNNN",
                    "Jumps to the address NNN plus V0.",
                    value =>
                        {
                            PC = (ushort)(value.ConvertNNN() + this.V[0]);
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "CXNN",
                    "Sets VX to a random number and NN.",
                    value =>
                        {
                            V[value.ConvertX()] = (byte)(new Random().Next() & value.ConvertNN());
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "DXYN",
                    "Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels."
                    + " Each row of 8 pixels is read as bit-coded (with the most significant bit of each byte displayed "
                    + "on the left) starting from memory location I; I value doesn't change after the execution of this "
                    + "instruction. As described above, VF is set to 1 if any screen pixels are flipped from set to unset "
                    + "when the sprite is drawn, and to 0 if that doesn't happen.",
                    value =>
                        {
                            ushort x = V[value.ConvertX()];
                            ushort y = V[value.ConvertY()];
                            ushort height = V[value.ConvertN()];

                            Vf = 0;
                            for (var yLine = 0; yLine < height; yLine++)
                            {
                                ushort pixel = Peripherals[Gfx].Memory[I + yLine];
                                for (var xLine = 0; xLine < 8; xLine++)
                                {
                                    if ((pixel & (0x80 >> xLine)) != 0)
                                    {
                                        if (Peripherals[Gfx].Memory[(x + xLine + ((y + yLine) * 64))] == 1) Vf = 1;
                                        Peripherals[Gfx].Memory[x + xLine + ((y + yLine) * 64)] ^= 1;
                                    }
                                }
                            }

                            DrawFlag = true;
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "EX9E",
                    "Skips the next instruction if the key stored in VX is pressed.",
                    value =>
                        {
                            if (V[value.ConvertX()] != 0) PC += PcIncrement * 2;
                            else PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "EXA1",
                    "Skips the next instruction if the key stored in VX isn't pressed.",
                    value =>
                        {
                            if (V[value.ConvertX()] == 0) PC += PcIncrement * 2;
                            else PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "FX07",
                    "Sets VX to the value of the delay timer.",
                    value =>
                        {
                            V[value.ConvertX()] = _delayTimer;
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "FX0A",
                    "A key press is awaited, and then stored in VX.",
                    value =>
                        {
                            while (PressedKey != 16)
                            {
                            }

                            V[value.ConvertX()] = PressedKey;
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "FX15",
                    "Sets the delay timer to VX.",
                    value =>
                        {
                            _delayTimer = V[value.ConvertX()];
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "FX18",
                    "Sets the sound timer to VX.",
                    value =>
                        {
                            _soundTimer = V[value.ConvertX()];
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "FX1E",
                    "Adds VX to I.",
                    value =>
                        {
                            I += V[value.ConvertX()];
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "FX29",
                    "Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented "
                    +"by a 4x5 font.",
                    value =>
                        {
                            I = V[value.ConvertX()];
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "FX33",
                    @"Stores the Binary-coded decimal representation of VX, with the most significant of three digits "
                    + "at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. "
                    + "(In other words, take the decimal representation of VX, place the hundreds digit in memory at location "
                    + "in I, the tens digit at location I+1, and the ones digit at location I+2.)",
                    value =>
                        {
                            var valueX = value.ConvertX();
                            Memory[I] = (byte)(valueX / 100);
                            Memory[I + 1] = (byte)((valueX / 10) % 10);
                            Memory[I + PcIncrement] = (byte)((valueX % 100) % 10);
                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "FX55",
                    "Stores V0 to VX in memory starting at address I.",
                    value =>
                        {
                            var count = value.ConvertX() + 1;
                            var index = 0;
                            foreach (var register in Registers.ToList().GetRange(0, count))
                            {
                                Memory[I + index] = register;
                                index++;
                            }

                            PC += PcIncrement;
                        }));
            this.OpCodes.Add(
                new OpCode(
                    "FX65",
                    "Fills V0 to VX with values from memory starting at address I.",
                    value =>
                        {
                            var count = V.Length - value.ConvertX();
                            var index = 0;
                            foreach (var memory in Memory.Memory.ToList().GetRange(I, count))
                            {
                                V[index] = memory;
                                index++;
                            }

                            PC += PcIncrement;
                        }));
        }

        /// <summary>
        ///     The setup input.
        /// </summary>
        private void SetupInput()
        {
            this._keyDefinition = new List<KeyMap>
                                      {
                                          new KeyMap('1', 1), 
                                          new KeyMap('2', 2), 
                                          new KeyMap('3', 3), 
                                          new KeyMap('4', 4), 
                                          new KeyMap('q', 5), 
                                          new KeyMap('w', 6), 
                                          new KeyMap('e', 7), 
                                          new KeyMap('r', 8), 
                                          new KeyMap('a', 9), 
                                          new KeyMap('s', 10), 
                                          new KeyMap('d', 11), 
                                          new KeyMap('f', 12), 
                                          new KeyMap('z', 13), 
                                          new KeyMap('x', 14), 
                                          new KeyMap('c', 15), 
                                          new KeyMap('v', 16), 
                                      };
        }


        private void LoadFontSet()
        {
            Memory[FontSet] = new[]
                                  {
                                      "0xF0".ToByte(),
                                      "0x90".ToByte(),
                                      "0x90".ToByte(),
                                      "0x90".ToByte(),
                                      "0xF0".ToByte(), // 0
                                      "0x20".ToByte(),
                                      "0x60".ToByte(),
                                      "0x20".ToByte(),
                                      "0x20".ToByte(),
                                      "0x70".ToByte(), // 1
                                      "0xF0".ToByte(),
                                      "0x10".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x80".ToByte(),
                                      "0xF0".ToByte(), // 2
                                      "0xF0".ToByte(),
                                      "0x10".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x10".ToByte(),
                                      "0xF0".ToByte(), // 3
                                      "0x90".ToByte(),
                                      "0x90".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x10".ToByte(),
                                      "0x10".ToByte(), // 4
                                      "0xF0".ToByte(),
                                      "0x80".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x10".ToByte(),
                                      "0xF0".ToByte(), // 5
                                      "0xF0".ToByte(),
                                      "0x80".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x90".ToByte(),
                                      "0xF0".ToByte(), // 6
                                      "0xF0".ToByte(),
                                      "0x10".ToByte(),
                                      "0x20".ToByte(),
                                      "0x40".ToByte(),
                                      "0x40".ToByte(), // 7
                                      "0xF0".ToByte(),
                                      "0x90".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x90".ToByte(),
                                      "0xF0".ToByte(), // 8
                                      "0xF0".ToByte(),
                                      "0x90".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x10".ToByte(),
                                      "0xF0".ToByte(), // 9
                                      "0xF0".ToByte(),
                                      "0x90".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x90".ToByte(),
                                      "0x90".ToByte(), // A
                                      "0xE0".ToByte(),
                                      "0x90".ToByte(),
                                      "0xE0".ToByte(),
                                      "0x90".ToByte(),
                                      "0xE0".ToByte(), // B
                                      "0xF0".ToByte(),
                                      "0x80".ToByte(),
                                      "0x80".ToByte(),
                                      "0x80".ToByte(),
                                      "0xF0".ToByte(), // C
                                      "0xE0".ToByte(),
                                      "0x90".ToByte(),
                                      "0x90".ToByte(),
                                      "0x90".ToByte(),
                                      "0xE0".ToByte(), // D
                                      "0xF0".ToByte(),
                                      "0x80".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x80".ToByte(),
                                      "0xF0".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x80".ToByte(),
                                      "0xF0".ToByte(),
                                      "0x80".ToByte(),
                                      "0x80".ToByte()
                                  };
        }


        #endregion
    }

    //public static class Filter
    //{
    //    public const ushort NNN = 4095;
    //    public const ushort NN = 255;
    //    public const ushort N = 15;
    //    public const ushort X = 3840;
    //    public const ushort Y = 240;
    //}
}