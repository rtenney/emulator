﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Wpf
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public partial class App
    {
    }
}