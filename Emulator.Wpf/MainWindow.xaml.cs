﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Wpf
{
    #region Usings

    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Security.Permissions;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    using Emulator.Chip8;
    using Emulator.Wpf.Annotations;

    #endregion

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public partial class MainWindow : INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        ///     The _chip 8.
        /// </summary>
        private readonly Machine _chip8;

        /// <summary>
        ///     The _screen.
        /// </summary>
        private ImageSource _screen;

        #endregion

        private BackgroundWorker _chip8BackgroundWorker;

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainWindow" /> class.
        /// </summary>
        public MainWindow()
        {
            this._chip8BackgroundWorker = new BackgroundWorker();
            this.InitializeComponent();
            this._chip8 = new Machine();
            this._chip8.LoadImage("HIDDEN");
            this._chip8.UpdateScreen += this.Chip8UpdateScreen;

            this._chip8BackgroundWorker.DoWork += (sender, args) => _chip8.RunEmulation(this.DoEvents);
            this._chip8BackgroundWorker.RunWorkerAsync();
            //this._chip8.RunEmulation(() => this.DoEvents());
        }

        #endregion

        #region Public Events

        /// <summary>
        ///     The property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the screen.
        /// </summary>
        public ImageSource Screen
        {
            get
            {
                return this._screen;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The do events.
        /// </summary>
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void DoEvents()
        {
            var frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new DispatcherOperationCallback(this.ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        /// <summary>
        /// The exit frame.
        /// </summary>
        /// <param name="f">
        /// The f.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on property changed.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// The button_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this._chip8.IsRunning = true;
        }

        /// <summary>
        /// The _chip 8_ update screen.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Chip8UpdateScreen(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(
                (Action)delegate
                    {
                        var chip8 = sender as Machine;
                        if (chip8 == null) return;

                        var width = 64; // for example
                        var height = 32; // for example
                        var dpiX = 96d;
                        var dpiY = 96d;
                        var pixelFormat = PixelFormats.BlackWhite; // grayscale bitmap
                        var bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8; // == 1 in this example
                        var stride = bytesPerPixel * width; // == width in this example

                        var bitmap = BitmapSource.Create(width, height, dpiX, dpiY, pixelFormat, null, chip8.Peripherals["gfx"].Memory.Memory, stride);
                        this._screen = bitmap;
                        this.OnPropertyChanged("Screen");
                    });
        }

        #endregion
    }
}