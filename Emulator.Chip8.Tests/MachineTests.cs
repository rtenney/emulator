﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MachineTests.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Chip8.Tests
{
    #region Usings

    using NUnit.Framework;

    #endregion

    /// <summary>
    ///     The machine tests.
    /// </summary>
    [TestFixture]
    public class MachineTests
    {
        #region Fields

        /// <summary>
        /// The _source.
        /// </summary>
        private Machine _source;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The initialize.
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            this._source = new Machine();
        }

        /// <summary>
        /// The load image test.
        /// </summary>
        [Test]
        public void LoadImageTest()
        {
            this._source.LoadImage("HIDDEN");
        }

        /// <summary>
        /// The run emulation test.
        /// </summary>
        [Test]
        public void RunEmulationTest()
        {
            this._source.LoadImage("HIDDEN");
            this._source.IsRunning = true;
            this._source.RunEmulation(null);
        }

        #endregion
    }
}