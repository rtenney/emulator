﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Range.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    /// <summary>
    ///     The range.
    /// </summary>
    public class Range
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the begin.
        /// </summary>
        public ushort Begin { get; set; }

        /// <summary>
        ///     Gets or sets the end.
        /// </summary>
        public ushort End { get; set; }

        /// <summary>
        ///     Gets the length.
        /// </summary>
        public int Length
        {
            get
            {
                return this.End - this.Begin;
            }
        }

        #endregion
    }
}