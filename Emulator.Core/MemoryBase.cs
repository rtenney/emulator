﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MemoryBase.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;

    #endregion

    /// <summary>
    ///     A class representing a base system memory.
    /// </summary>
    public class MemoryBase
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MemoryBase" /> class.
        ///     Initializes with 4096 bytes of memory.
        /// </summary>
        public MemoryBase()
            : this(4096)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryBase"/> class.
        /// </summary>
        /// <param name="size">
        /// The initial memory size.
        /// </param>
        public MemoryBase(int size)
        {
            this.Memory = new byte[size];
            this.MemoryMap = new Dictionary<string, Range>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the memory.
        /// </summary>
        /// <value>
        ///     The memory.
        /// </value>
        public byte[] Memory { get; set; }

        /// <summary>
        ///     Gets the memory map.
        /// </summary>
        public Dictionary<string, Range> MemoryMap { get; private set; }

        #endregion

        #region Public Indexers

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <returns>
        /// The <see cref="byte"/>.
        /// </returns>
        public byte this[int index]
        {
            get
            {
                return this.Memory[index];
            }

            set
            {
                this.Memory[index] = value;
            }
        }

        public byte[] this[string region]
        {
            get
            {
                var reg = MemoryMap[region];
                byte[] mem = new byte[reg.Length];
                int i = 0;
                for (int index = reg.Begin; index < reg.End; index++)
                {
                    mem[i] = this.Memory[index];
                    i++;
                }

                return mem;
            }

            set
            {
                var reg = MemoryMap[region];
                int i = 0;
                for (int index = reg.Begin; index < reg.End; index++)
                {
                    this.Memory[index] = value[i];
                    i++;
                }
            }
        }

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="region">
        /// The region.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <exception cref="IndexOutOfRangeException">
        /// Throws IndexOutOfRangeException if the index is not in the region's range.
        /// </exception>
        /// <returns>
        /// The <see cref="byte"/>.
        /// </returns>
        public byte this[string region, int index]
        {
            get
            {
                return Memory[CheckBounds(region, index)];
            }

            set
            {
                var i = this.CheckBounds(region, index);

                Memory[i] = value;
            }
        }

        private int CheckBounds(string region, int index)
        {
            if (string.IsNullOrEmpty(region) || !this.MemoryMap.ContainsKey(region))
            {
                return index;
            }

            var range = this.MemoryMap[region];

            if (index < 0 || index > this.Memory.Length || index + range.Begin >= this.Memory.Length)
            {
                throw new IndexOutOfRangeException(
                    string.Format(
                        "Index for region: {0} is out of range.{1}Index: {4}{1}Range begin: {2}{1}Range end: {3}",
                        region,
                        Environment.NewLine,
                        range.Begin,
                        range.End,
                        index));
            }

            //if (index < range.Begin || index > range.End)
            //{
            //    throw new IndexOutOfRangeException(
            //        string.Format(
            //            "Index for region: {0} is out of range.{1}Index: {4}{1}Range begin: {2}{1}Range end: {3}",
            //            region,
            //            Environment.NewLine,
            //            range.Begin,
            //            range.End,
            //            index));
            //}
            return index + range.Begin;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Clears the memory.
        /// </summary>
        /// <param name="start">
        /// The starting index.
        /// </param>
        /// <param name="length">
        /// The length to clear.
        /// </param>
        /// <returns>
        /// True if successful, false otherwise.
        /// </returns>
        public bool ClearMemory(int start, int length)
        {
            var size = this.Memory.Length;
            if (start > size || start + length > size)
            {
                return false;
            }

            try
            {
                Array.Clear(this.Memory, start, length);
                return true;
            }
            catch (Exception ex)
            {
                Messenger.Announce(Messenger.MessageType.Error, Messenger.MessagePriority.High, "Error while attempting to Clear memory: " + Environment.NewLine + ex);
                return false;
            }
        }

        /// <summary>
        ///     Clears the memory.
        /// </summary>
        public void ClearMemory()
        {
            this.ClearMemory(0, this.Memory.Length);
        }

        /// <summary>
        /// The get memory as byte.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="littleEndian">
        /// Set to true to use little endian.
        /// </param>
        /// <returns>
        /// The <see cref="byte"/>.
        /// </returns>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        public byte GetMemoryAsByte(int index, bool littleEndian)
        {
            return this.GetMemoryAsByteArray(index, DataSize.Byte, littleEndian)[0];
        }

        /// <summary>
        /// The get memory as byte.
        /// </summary>
        /// <param name="region">
        /// The region.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="littleEndian">
        /// Set to true to use little endian.
        /// </param>
        /// <returns>
        /// The <see cref="byte"/>.
        /// </returns>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        public byte GetMemoryAsByte(string region, int index, bool littleEndian)
        {
            return this.GetMemoryAsByteArray(region, index, DataSize.Byte, littleEndian)[0];
        }

        /// <summary>
        /// Gets a byte array from the memory.
        /// </summary>
        /// <param name="index">
        /// The index of the memory.
        /// </param>
        /// <param name="length">
        /// The length to retrieve.
        /// </param>
        /// <param name="littleEndian">
        /// Use LittleEndian
        /// </param>
        /// <returns>
        /// A byte of the memory at the index location.
        /// </returns>
        public byte[] GetMemoryAsByteArray(int index, DataSize length = DataSize.Byte, bool littleEndian = false)
        {
            var size = (int)length;
            var result = new byte[size];

            for (var i = 0; i < size; i++)
            {
                result[i] = this.Memory[index + i];
            }

            if (BitConverter.IsLittleEndian || littleEndian)
            {
                Array.Reverse(result);
            }

            return result;
        }

        /// <summary>
        /// Gets a byte array from the memory.
        /// </summary>
        /// <param name="region">
        /// The region.
        /// </param>
        /// <param name="index">
        /// The index of the memory.
        /// </param>
        /// <param name="length">
        /// The length to retrieve.
        /// </param>
        /// <param name="littleEndian">
        /// Use LittleEndian
        /// </param>
        /// <returns>
        /// A byte of the memory at the index location.
        /// </returns>
        public byte[] GetMemoryAsByteArray(string region, int index, DataSize length = DataSize.Byte, bool littleEndian = false)
        {
            var size = (int)length;
            var result = new byte[size];

            for (var i = 0; i < size; i++)
            {
                result[i] = this[region, index + i];
            }

            if (BitConverter.IsLittleEndian || littleEndian)
            {
                Array.Reverse(result);
            }

            return result;
        }

        /// <summary>
        /// The get memory as double word.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="littleEndian">
        /// Set to true to use little endian.
        /// </param>
        /// <returns>
        /// The <see cref="uint"/>.
        /// </returns>
        public uint GetMemoryAsDoubleWord(int index, bool littleEndian)
        {
            return BitConverter.ToUInt32(this.GetMemoryAsByteArray(index, DataSize.DoubleWord, littleEndian), 0);
        }

        /// <summary>
        /// The get memory as double word.
        /// </summary>
        /// <param name="region">
        /// The region.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="littleEndian">
        /// Set to true to use little endian.
        /// </param>
        /// <returns>
        /// The <see cref="uint"/>.
        /// </returns>
        public uint GetMemoryAsDoubleWord(string region, int index, bool littleEndian)
        {
            return BitConverter.ToUInt32(this.GetMemoryAsByteArray(region, index, DataSize.DoubleWord, littleEndian), 0);
        }

        /// <summary>
        /// The get memory as quad word.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="littleEndian">
        /// Set to true to use little endian.
        /// </param>
        /// <returns>
        /// The <see cref="ulong"/>.
        /// </returns>
        public ulong GetMemoryAsQuadWord(int index, bool littleEndian)
        {
            return BitConverter.ToUInt64(this.GetMemoryAsByteArray(index, DataSize.QuadWord, littleEndian), 0);
        }

        /// <summary>
        /// The get memory as quad word.
        /// </summary>
        /// <param name="region">
        /// The region.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="littleEndian">
        /// Set to true to use little endian.
        /// </param>
        /// <returns>
        /// The <see cref="ulong"/>.
        /// </returns>
        public ulong GetMemoryAsQuadWord(string region, int index, bool littleEndian = false)
        {
            return BitConverter.ToUInt64(this.GetMemoryAsByteArray(region, index, DataSize.QuadWord, littleEndian), 0);
        }

        /// <summary>
        /// The get memory as word.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="littleEndian">
        /// Set to true to use little endian.
        /// </param>
        /// <returns>
        /// The <see cref="ushort"/>.
        /// </returns>
        public ushort GetMemoryAsWord(int index, bool littleEndian = false)
        {
            return BitConverter.ToUInt16(this.GetMemoryAsByteArray(index, DataSize.Word, littleEndian), 0);
        }

        /// <summary>
        /// The get memory as word.
        /// </summary>
        /// <param name="region">
        /// The region.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="littleEndian">
        /// Set to true to use little endian.
        /// </param>
        /// <returns>
        /// The <see cref="ushort"/>.
        /// </returns>
        public ushort GetMemoryAsWord(string region, int index, bool littleEndian)
        {
            return BitConverter.ToUInt16(this.GetMemoryAsByteArray(region, index, DataSize.Word, littleEndian), 0);
        }

        /// <summary>
        /// Resizes the memory with specified new size.
        /// </summary>
        /// <param name="newSize">
        /// The new size.
        /// </param>
        /// <param name="preserveData">
        /// if set to <c>true</c> will preserve the data in memory.
        /// </param>
        /// <returns>
        /// True if successful, false otherwise. Will also return false if the new memory size is smaller than current and
        ///     preserveData is set to true.
        /// </returns>
        public bool Resize(int newSize, bool preserveData = false)
        {
            if (newSize == this.Memory.Length)
            {
                return false;
            }

            try
            {
                if (preserveData)
                {
                    if (newSize < this.Memory.Length)
                    {
                        return false;
                    }

                    var temp = (byte[])this.Memory.Clone();
                    this.Memory = new byte[newSize];
                    for (var i = 0; i < temp.Length; i++)
                    {
                        this.Memory[i] = temp[i];
                    }

                    return true;
                }

                this.Memory = new byte[newSize];
                return true;
            }
            catch (Exception ex)
            {
                Messenger.Announce(Messenger.MessageType.Error, Messenger.MessagePriority.High, "Error while attempting to Resize memory: " + Environment.NewLine + ex);
                return false;
            }
        }

        #endregion
    }
}