﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    #region Usings

    using System;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public static class Extensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get value.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="format">
        /// The format.
        /// </param>
        /// <returns>
        /// The <see cref="ushort[]"/>.
        /// </returns>
        public static ushort[] GetValue(this string value, string format)
        {
            var result = new List<string>();
            var finalResult = new List<ushort>();
            var listIndex = -1;
            bool changed = false;
            char previous = default(char);
            for (var index = 0; index < format.Length; index++)
            {
                var character = format[index];
                var val = value[index];

                if (previous == default(char))
                {
                    previous = character;
                    changed = false;
                }
                else if (!previous.Equals(character))
                {
                    previous = character;
                    changed = true;
                }
                else if (previous == character) changed = false;

                if (!val.Equals(character) && !changed) result[listIndex] += val;

                if (!val.Equals(character) && changed)
                {
                    listIndex++;
                    result.Add(string.Empty);
                    result[listIndex] += val;
                    changed = false;
                }
            }

            foreach (var val2 in result) finalResult.Add(Convert.ToUInt16(val2, 16));

            return finalResult.ToArray();
        }

        /// <summary>
        /// The take after.
        /// </summary>
        /// <param name="s">
        /// The s.
        /// </param>
        /// <param name="searchFor">
        /// The search for.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string TakeAfter(this string s, string searchFor)
        {
            return TakeFrom(s, searchFor).Substring(1);
        }

        /// <summary>
        /// Returns the contents of a string starting with the location of the searchFor
        /// </summary>
        /// <param name="s">
        /// The string to search.
        /// </param>
        /// <param name="searchFor">
        /// The string to search for.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string TakeFrom(this string s, string searchFor)
        {
            if (s.Contains(searchFor))
            {
                int length = Math.Max(s.Length, 0);

                int index = s.IndexOf(searchFor);

                return s.Substring(index, length - index);
            }

            return s;
        }

        /// <summary>
        /// The to byte.
        /// </summary>
        /// <param name="hexValue">
        /// The hex value.
        /// </param>
        /// <returns>
        /// The <see cref="byte"/>.
        /// </returns>
        public static byte ToByte(this string hexValue)
        {
            return Convert.ToByte(hexValue.TakeAfter("x"), 16);
        }

        /// <summary>
        /// The to int.
        /// </summary>
        /// <param name="hexValue">
        /// The hex value.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public static int ToInt(this string hexValue)
        {
            return Convert.ToInt16(hexValue.TakeAfter("x"), 16);
        }

        /// <summary>
        /// The to ushort.
        /// </summary>
        /// <param name="hexValue">
        /// The hex value.
        /// </param>
        /// <returns>
        /// The <see cref="ushort"/>.
        /// </returns>
        public static ushort ToUshort(this string hexValue)
        {
            return Convert.ToUInt16(hexValue.TakeAfter("x"), 16);
        }

        public static ushort ConvertNNN(this ushort value)
        {
            return (ushort)(value & 4095);
        }


        public static ushort ConvertNN(this ushort value)
        {
            return (ushort)(value & 255);
        }


        public static ushort ConvertN(this ushort value)
        {
            return (ushort)(value & 15);
        }


        public static ushort ConvertX(this ushort value)
        {
            return (ushort)((value & 3840) >> 8);
        }

        public static ushort ConvertY(this ushort value)
        {
            return (ushort)((value & 240) >> 4);
        }

        #endregion
    }
}