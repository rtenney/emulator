﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeripheralBase.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public class PeripheralBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PeripheralBase"/> class.
        /// </summary>
        /// <param name="memorySize">
        /// The memory size.
        /// </param>
        /// <param name="numberOfRegisters">
        /// The number of registers.
        /// </param>
        public PeripheralBase(int memorySize, int numberOfRegisters, int stackSize)
        {
            this.IndexRegister = 0;
            this.Memory = new MemoryBase(memorySize);
            this.ProgramCounter = 0;
            this.V = new byte[numberOfRegisters];
            this.OpCodes = new OpCodeCollection();
            this.Stack = new ushort[stackSize];
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PeripheralBase" /> class.
        /// </summary>
        public PeripheralBase()
            : this(4096, 16, 16)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PeripheralBase"/> class.
        /// </summary>
        /// <param name="memorySize">
        /// The memory size.
        /// </param>
        public PeripheralBase(int memorySize)
            : this(memorySize, 16, 16)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the Index Register.
        /// </summary>
        public ushort I
        {
            get
            {
                return this.IndexRegister;
            }

            set
            {
                this.IndexRegister = value;
            }
        }

        /// <summary>
        ///     Gets or sets the index register.
        /// </summary>
        public ushort IndexRegister { get; set; }

        /// <summary>
        ///     Gets or sets the memory.
        /// </summary>
        public MemoryBase Memory { get; set; }

        /// <summary>
        ///     Gets or sets the op code.
        /// </summary>
        public OpCode OpCode { get; set; }

        /// <summary>
        ///     Gets or sets the op codes.
        /// </summary>
        public OpCodeCollection OpCodes { get; set; }

        /// <summary>
        ///     Gets or sets the program counter.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public ushort PC
        {
            get
            {
                return this.ProgramCounter;
            }

            set
            {
                this.ProgramCounter = value;
            }
        }

        public ushort[] Stack { get; set; }

        public int StackPointer { get; set; }

        public int SP
        {
            get
            {
                return StackPointer;
            }

            set
            {
                StackPointer = value;
            }
        }

        /// <summary>
        ///     Gets or sets the program counter.
        /// </summary>
        public ushort ProgramCounter { get; set; }

        /// <summary>
        ///     Gets or sets the registers.
        /// </summary>
        public byte[] V {
            get
            {
                return this.Registers;
            }
            set
            {
                this.Registers = value;
            }
        }
        public byte[] Registers { get; set; }
        #endregion
    }
}