﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessengerArguments.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    #region Usings

    using System;

    #endregion

    /// <summary>
    ///     The messenger arguments.
    /// </summary>
    public class MessengerArguments
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the callback action.
        /// </summary>
        public Action<object[]> CallbackAction { get; set; }

        /// <summary>
        ///     Gets or sets the message priority.
        /// </summary>
        public Messenger.MessagePriority MessagePriority { get; set; }

        /// <summary>
        ///     Gets or sets the message type.
        /// </summary>
        public Messenger.MessageType MessageType { get; set; }

        /// <summary>
        ///     Gets or sets the sender.
        /// </summary>
        public object Sender { get; set; }

        /// <summary>
        ///     Gets or sets the token.
        /// </summary>
        public object Token { get; set; }

        #endregion
    }
}