﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReadOnlyDictionary.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    #region Usings

    using System;
    using System.Collections;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    /// <typeparam name="TKey">
    /// The Key
    /// </typeparam>
    /// <typeparam name="TValue">
    /// The Value
    /// </typeparam>
    public class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        #region Fields

        /// <summary>
        ///     The _dictionary.
        /// </summary>
        private readonly IDictionary<TKey, TValue> _dictionary;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReadOnlyDictionary{TKey,TValue}" /> class.
        /// </summary>
        public ReadOnlyDictionary()
        {
            this._dictionary = new Dictionary<TKey, TValue>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadOnlyDictionary{TKey,TValue}"/> class.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary.
        /// </param>
        public ReadOnlyDictionary(IDictionary<TKey, TValue> dictionary)
        {
            this._dictionary = dictionary;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the count.
        /// </summary>
        public int Count
        {
            get
            {
                return this._dictionary.Count;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether is read only.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        ///     Gets the keys.
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                return this._dictionary.Keys;
            }
        }

        /// <summary>
        ///     Gets the values.
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                return this._dictionary.Values;
            }
        }

        #endregion

        #region Public Indexers

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="TValue"/>.
        /// </returns>
        public TValue this[TKey key]
        {
            get
            {
                return this._dictionary[key];
            }
        }

        #endregion

        #region Explicit Interface Indexers

        /// <summary>
        /// Retrieves the value at the given index.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <exception cref="Exception">
        /// Throws a ReadOnlyException if you try to set this.
        /// </exception>
        /// <returns>
        /// The <see cref="TValue"/>.
        ///     The value
        /// </returns>
        TValue IDictionary<TKey, TValue>.this[TKey key]
        {
            get
            {
                return this[key];
            }

            set
            {
                throw ReadOnlyException();
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The contains.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return this._dictionary.Contains(item);
        }

        /// <summary>
        /// The contains key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ContainsKey(TKey key)
        {
            return this._dictionary.ContainsKey(key);
        }

        /// <summary>
        /// The copy to.
        /// </summary>
        /// <param name="array">
        /// The array.
        /// </param>
        /// <param name="arrayIndex">
        /// The array index.
        /// </param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            this._dictionary.CopyTo(array, arrayIndex);
        }

        /// <summary>
        ///     The get enumerator.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerator" />.
        /// </returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return this._dictionary.GetEnumerator();
        }

        /// <summary>
        /// The try get value.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            return this._dictionary.TryGetValue(key, out value);
        }

        #endregion

        #region Explicit Interface Methods

        /// <summary>
        /// Add a new value to the given key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <exception cref="Exception">
        /// Throws a ReadOnlyException if you try to add to this.
        /// </exception>
        void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
        {
            throw ReadOnlyException();
        }

        /// <summary>
        /// Add a new value to the given key.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <exception cref="Exception">
        /// Throws a ReadOnlyException if you try to add to this.
        /// </exception>
        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            throw ReadOnlyException();
        }

        /// <summary>
        ///     Clears the dictionary.
        /// </summary>
        /// <exception cref="Exception">
        ///     Throws a ReadOnlyException if you try to clear this.
        /// </exception>
        void ICollection<KeyValuePair<TKey, TValue>>.Clear()
        {
            throw ReadOnlyException();
        }

        /// <summary>
        ///     The get enumerator.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerator" />.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Remove a key from the dictionary.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws a ReadOnlyException if you try to remove from this.
        /// </exception>
        bool IDictionary<TKey, TValue>.Remove(TKey key)
        {
            throw ReadOnlyException();
        }

        /// <summary>
        /// Remove an item from the dictionary.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws a ReadOnlyException if you try to remove from this.
        /// </exception>
        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            throw ReadOnlyException();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The read only exception.
        /// </summary>
        /// <returns>
        ///     The <see cref="Exception" />.
        /// </returns>
        private static Exception ReadOnlyException()
        {
            return new NotSupportedException("This dictionary is read-only");
        }

        #endregion
    }
}