﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataSize.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    /// <summary>
    /// The data size.
    /// </summary>
    public enum DataSize
    {
        /// <summary>
        /// The byte.
        /// </summary>
        Byte = 1, 

        /// <summary>
        /// The word.
        /// </summary>
        Word = 2, 

        /// <summary>
        /// The double word.
        /// </summary>
        DoubleWord = Word * 2, 

        /// <summary>
        /// The quad word.
        /// </summary>
        QuadWord = Word * 4
    }
}