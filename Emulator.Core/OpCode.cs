﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OpCode.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    #region Usings

    using System;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public class OpCode
    {
        #region Fields

        /// <summary>
        ///     The _value.
        /// </summary>
        private string _value = string.Empty;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OpCode"/> class.
        /// </summary>
        /// <param name="format">
        /// The format.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="commandActionParam">
        /// The command Action Single.
        /// </param>
        public OpCode(string format, string description, Action<ushort> commandActionParam)
        {
            this.CommandActionParam = commandActionParam;
            this.Description = description;
            this.Format = format;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpCode"/> class.
        /// </summary>
        /// <param name="format">
        /// The format.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="commandAction">
        /// The command action.
        /// </param>
        public OpCode(string format, string description, Action commandAction)
        {
            this.CommandAction = commandAction;
            this.Description = description;
            this.Format = format;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpCode"/> class.
        /// </summary>
        /// <param name="format">
        /// The format.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public OpCode(string format, string description)
        {
            this.Description = description;
            this.Format = format;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the command action.
        /// </summary>
        public Action CommandAction { get; private set; }

        /// <summary>
        ///     Gets the command action with parameters.
        /// </summary>
        public Action<ushort> CommandActionParam { get; private set; }

        /// <summary>
        ///     Gets the description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        ///     Gets the format.
        /// </summary>
        public string Format { get; private set; }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this._value;
            }

            set
            {
                var s = this._value;
                if (s != null && (value != null && (s.Equals(value) || value.Length > 4)))
                {
                    return;
                }

                if (this.CheckFormat(value))
                {
                    this._value = value;
                }
                else
                {
                    throw new ArgumentException("The provided value is not supported by this OpCode.", "Value");
                }
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The execute command.
        /// </summary>
        public void ExecuteCommand()
        {
            if ((this.CommandAction == null 
                && this.CommandActionParam == null)
                || this.Value == null)
            {
                return;
            }

            if (this.CommandAction != null)
            {
                this.CommandAction.Invoke();
            }
            else if (this.CommandActionParam != null)
            {
                this.CommandActionParam.Invoke(Convert.ToUInt16(this.Value, 16));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The check format.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CheckFormat(string value)
        {
            return value[0].Equals(this.Format[0]);
        }

        #endregion
    }

}