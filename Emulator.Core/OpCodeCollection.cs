﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OpCodeCollection.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Linq;

    #endregion

    /// <summary>
    ///     The op code collection.
    /// </summary>
    public class OpCodeCollection : List<OpCode>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="OpCodeCollection" /> class.
        /// </summary>
        public OpCodeCollection()
            : base(new List<OpCode>())
        {
        }

        #endregion

        /// <summary>
        /// The find with code.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <returns>
        /// The <see cref="OpCode"/>.
        /// </returns>
        public OpCode FindWithCode(string code)
        {
            var opCodes = this.Where(opCode => opCode.Format[0] == code[0]).ToList();

            switch (opCodes.Count)
            {
                case 0:
                    return null;
                case 1:
                    return opCodes.First();
                default:
                    var secondList = opCodes.Where(opCode => opCode.Format[3] == code[3]).ToList();

                    switch (secondList.Count)
                    {
                        case 0:
                            return null;
                        case 1:
                            return secondList.First();
                        default:
                            var thirdList = opCodes.Where(opCode => opCode.Format[2] == code[2]).ToList();

                            switch (thirdList.Count)
                            {
                                case 0:
                                    return null;
                                case 1:
                                    return thirdList.First();
                            }

                            break;

                    }

                    break;
            }

            return null;
            ////if (code == "00E0") return this.FirstOrDefault(opCode => opCode.Format == "00E0");
            ////if (code == "00EE") return this.FirstOrDefault(opCode => opCode.Format == "00EE");

            ////return (from op in this
            ////        let opMask = Convert.ToUInt16(GetMask(op.Format), 16)
            ////        where (opMask & Convert.ToUInt16(code, 16)) == opMask
            ////        select op).FirstOrDefault();

            ////foreach (var op in this.Where(op => op.Format[0].Equals(code[0])))
            ////{
            ////    for (var i = 1; i < 4; i++)
            ////    {
            ////        var c = op.Format[i];
            ////        if (c.Equals('X') || c.Equals('N') || c.Equals('Y'))
            ////            if (i != 3) continue;
            ////            else return op;

            ////        if (!c.Equals(code[i])) 
            ////            break;

            ////        if (i == 3) return op;
            ////    }
            ////}
        }

        public static string GetMask(string code)
        {
            var codeMask = code.Substring(0,1);
            foreach (var c in code.Substring(1))
                if (c.Equals('X') || c.Equals('N') || c.Equals('Y')) codeMask += "F";
                else codeMask += "0";
            return codeMask;
        }
    }
}