﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MachineBase.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    #region Usings

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public class MachineBase : PeripheralBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MachineBase"/> class.
        /// </summary>
        /// <param name="memorySize">
        /// The memory size.
        /// </param>
        /// <param name="numberOfRegisters">
        /// The number of registers.
        /// </param>
        /// <param name="stackSize">Size to set the stack
        /// </param>
        public MachineBase(int memorySize, int numberOfRegisters, int stackSize)
            : base(memorySize, numberOfRegisters, stackSize)
        {
            this.Peripherals = new Dictionary<string, PeripheralBase>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the peripherals.
        /// </summary>
        public Dictionary<string, PeripheralBase> Peripherals { get; private set; }

        #endregion
    }
}