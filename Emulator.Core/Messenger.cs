﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Messenger.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    #endregion

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public static class Messenger
    {
        #region Static Fields

        /// <summary>
        /// The default token.
        /// </summary>
        public static readonly object DefaultToken = new object();

        /// <summary>
        /// The registry objects.
        /// </summary>
        private static readonly List<MessengerArguments> RegistryObjects;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="Messenger"/> class.
        /// </summary>
        static Messenger()
        {
            AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += (sender, args) => Assembly.ReflectionOnlyLoad(args.Name);
            RegistryObjects = new List<MessengerArguments>();
        }

        #endregion

        #region Enums

        /// <summary>
        /// The message priority.
        /// </summary>
        public enum MessagePriority
        {
            /// <summary>
            /// The low.
            /// </summary>
            Low, 

            /// <summary>
            /// The medium.
            /// </summary>
            Medium, 

            /// <summary>
            /// The high.
            /// </summary>
            High, 

            /// <summary>
            /// The severe.
            /// </summary>
            Severe
        }

        /// <summary>
        /// The message type.
        /// </summary>
        public enum MessageType
        {
            /// <summary>
            /// The message.
            /// </summary>
            Message, 

            /// <summary>
            /// The error.
            /// </summary>
            Error, 

            /// <summary>
            /// The information.
            /// </summary>
            Information, 

            /// <summary>
            /// The question.
            /// </summary>
            Question, 

            /// <summary>
            /// The response.
            /// </summary>
            Response, 

            /// <summary>
            /// The event.
            /// </summary>
            Event, 

            /// <summary>
            /// The action.
            /// </summary>
            Action
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The announce.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Announce(object token, object message)
        {
            Announce(token, MessageType.Message, MessagePriority.Low, message, null, null);
        }

        /// <summary>
        /// The announce.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public static void Announce(object token, object message, Action callback)
        {
            Announce(token, MessageType.Question, MessagePriority.Medium, message, callback, null);
        }

        /// <summary>
        /// The announce.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public static void Announce(object token, object message, Action<object> callback)
        {
            Announce(token, MessageType.Question, MessagePriority.Medium, message, null, callback);
        }

        /// <summary>
        /// The announce.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Announce(object message)
        {
            Announce(DefaultToken, message);
        }

        /// <summary>
        /// The announce.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="priority">
        /// The priority.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// <param name="callbackParameters">
        /// The callback parameters.
        /// </param>
        /// <param name="performCallbackWithoutRegistered">
        /// The perform callback without registered.
        /// </param>
        public static void Announce(
            object token, 
            MessageType type, 
            MessagePriority priority, 
            object message, 
            Action callback, 
            Action<object> callbackParameters, 
            bool performCallbackWithoutRegistered = true)
        {
            var i = 0;
            while (RegistryObjects.Count() != i)
            {
                // LogMessage(type, priority, message);
                if (RegistryObjects[i].Token == token)
                {
                    RegistryObjects[i].CallbackAction.Invoke(
                        new[]
                            {
                                message, 
                                type, 
                                priority, 
                                callback, 
                                callbackParameters
                            });
                }
                else
                {
                    if (performCallbackWithoutRegistered)
                    {
                        if (callback != null)
                        {
                            callback.Invoke();
                        }
                    }

                    if (callbackParameters != null)
                    {
                        callbackParameters.Invoke(null);
                    }

                    if (token is ErrorHandler)
                    {
                        DefaultErrorHandler(message);
                    }
                }

                i++;
            }
        }

        /// <summary>
        /// The announce.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="priority">
        /// The priority.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Announce(MessageType type, MessagePriority priority, string message)
        {
            switch (type)
            {
                case MessageType.Message:
                    break;
                case MessageType.Error:
                    Announce(new ErrorHandler(), type, priority, message, null, null);
                    break;
                case MessageType.Information:
                    break;
                case MessageType.Question:
                    break;
                case MessageType.Response:
                    break;
                case MessageType.Event:
                    break;
                case MessageType.Action:
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }

        /// <summary>
        /// The confirm changes.
        /// </summary>
        /// <param name="callback">
        /// The callback.
        /// </param>
        public static void ConfirmChanges(Action callback)
        {
            Announce("Confirm", "This record has unsaved data and will be lost if you proceed, Continue?", callback);
        }

        // private static void LogMessage(MessageType type, MessagePriority priority, object message)
        // {
        // string newVariable = message as string;
        // if (!String.IsNullOrEmpty(newVariable))
        // {
        // Log.Debug(newVariable);
        // switch (type)
        // {
        // case MessageType.Message:
        // Trace.TraceInformation(message.ToString());
        // break;
        // case MessageType.Error:
        // break;
        // case MessageType.Information:
        // break;
        // case MessageType.Question:
        // break;
        // case MessageType.Response:
        // break;
        // case MessageType.Event:
        // break;
        // case MessageType.Action:
        // break;
        // default:
        // throw new ArgumentOutOfRangeException("type");
        // }
        // }
        // }

        /// <summary>
        /// Registers a messenger callback with the specified sender.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="callbackAction">
        /// The callback action.
        /// </param>
        public static void Register(object sender, object token, Action<object> callbackAction)
        {
            Register(
                new MessengerArguments
                    {
                        Sender = sender, 
                        Token = token, 
                        MessageType = MessageType.Message, 
                        MessagePriority = MessagePriority.Low, 
                        CallbackAction = callbackAction
                    });
        }

        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="priority">
        /// The priority.
        /// </param>
        /// <param name="callbackAction">
        /// The callback action.
        /// </param>
        public static void Register(object sender, object token, MessageType type, MessagePriority priority, Action<object> callbackAction)
        {
            Register(new MessengerArguments { Sender = sender, Token = token, MessageType = type, MessagePriority = priority, CallbackAction = callbackAction });
        }

        /// <summary>
        /// Registers a messenger callback with the specified arguments.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        public static void Register(MessengerArguments args)
        {
            if (!RegistryObjects.Contains(args, new MessengerArgumentComparer()))
            {
                RegistryObjects.Add(args);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The default error handler.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        private static void DefaultErrorHandler(object message)
        {
            Console.WriteLine(message as string);
        }

        #endregion

        /// <summary>
        /// The messenger argument comparer.
        /// </summary>
        private class MessengerArgumentComparer : IEqualityComparer<MessengerArguments>
        {
            #region Public Methods and Operators

            /// <summary>
            /// Determines whether the specified objects are equal.
            /// </summary>
            /// <returns>
            /// true if the specified objects are equal; otherwise, false.
            /// </returns>
            /// <param name="x">
            /// The first object of type <see cref="MessengerArguments"/> to compare.
            /// </param>
            /// <param name="y">
            /// The second object of type <see cref="MessengerArguments"/> to compare.
            /// </param>
            public bool Equals(MessengerArguments x, MessengerArguments y)
            {
                return x.Sender == y.Sender && x.Token == y.Token;
            }

            /// <summary>
            /// Returns a hash code for the specified object.
            /// </summary>
            /// <returns>
            /// A hash code for the specified object.
            /// </returns>
            /// <param name="obj">
            /// The <see cref="T:System.Object"/> for which a hash code is to be returned.
            /// </param>
            /// <exception cref="T:System.ArgumentNullException">
            /// The type of <paramref name="obj"/> is a reference type and
            ///     <paramref name="obj"/> is null.
            /// </exception>
            public int GetHashCode(MessengerArguments obj)
            {
                return obj.Sender.GetHashCode() ^ obj.Token.GetHashCode() ^ obj.CallbackAction.GetHashCode();
            }

            #endregion
        }
    }
}