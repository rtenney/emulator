﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExtensionsTests.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core.Tests
{
    #region Usings

    using NUnit.Framework;

    #endregion

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    [TestFixture]
    public class ExtensionsTests
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The get value test.
        /// </summary>
        [Test]
        public void GetValueTest()
        {
            const string format = "ANNN";
            const string value = "A123";

            const ushort expected = 123;
            var actual = value.GetValue(format)[0];

            Assert.AreEqual(expected, actual, "Values do not match.");
        }

        [Test]
        public void ToIntTest()
        {
            const string value = "0x200";
            const int expected = 512;
            var actual = value.ToInt();

            Assert.AreEqual(expected, actual, "Conversion failed, values do not match.");
        }

        /// <summary>
        ///     The initialize.
        /// </summary>
        [SetUp]
        public void Initialize()
        {
        }

        #endregion
    }
}