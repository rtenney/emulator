﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MemoryBaseTests.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core.Tests
{
    #region Usings

    using System;
    using NUnit.Framework;

    #endregion

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    [TestFixture]
    public class MemoryBaseTests
    {
        #region Fields

        /// <summary>
        ///     The _original.
        /// </summary>
        private byte[] _original;

        /// <summary>
        ///     The _rand.
        /// </summary>
        private Random _rand;

        /// <summary>
        ///     The _source.
        /// </summary>
        private MemoryBase _source;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The clear memory test.
        /// </summary>
        [Test]
        public void ClearMemoryTest()
        {
            this._source = new MemoryBase();
            this.GenerateRandomData();
            this._source.ClearMemory();

            // Compare with original
            var matched = this.CompareWithOriginal();
            Assert.IsFalse(matched, "Array did not clear.");
        }

        /// <summary>
        ///     The get memory as byte array test.
        /// </summary>
        [Test]
        public void GetMemoryAsByteArrayTest()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            var index = this._rand.Next(0, this._source.Memory.Length);
            var expected = new[]
                               {
                                   this._source.Memory[index]
                               };

            var actual = this._source.GetMemoryAsByteArray(index);
            Assert.AreEqual(expected, actual, "Byte arrays do not match");
        }

        /// <summary>
        /// The get memory as byte array test 2.
        /// </summary>
        [Test]
        public void GetMemoryAsByteArrayTest2()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            const string region = "Rom";
            var index = this._rand.Next(0, this._source.MemoryMap[region].Length - 1);
            var expected = new[]
                               {
                                   this._source[region, index]
                               };

            var actual = this._source.GetMemoryAsByteArray(region, index);
            Assert.AreEqual(expected, actual, "Byte arrays do not match");
        }

        /// <summary>
        /// The get memory byte test 2.
        /// </summary>
        [Test]
        public void GetMemoryAsByteTest2()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            const string region = "Rom";
            var index = this._rand.Next(0, this._source.MemoryMap[region].Length - 1);
            var expected = this._source[region, index];

            var actual = this._source.GetMemoryAsByte(region, index, false);
            Assert.AreEqual(expected, actual, "Bytes do not match");
        }

        /// <summary>
        /// The get memory byte test.
        /// </summary>
        [Test]
        public void GetMemoryAsByteTest()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            var index = this._rand.Next(0, this._source.Memory.Length);
            var expected = this._source.Memory[index];

            var actual = this._source.GetMemoryAsByte(index, false);
            Assert.AreEqual(expected, actual, "Bytes do not match");
        }

        /// <summary>
        /// The get memory as word test.
        /// </summary>
        [Test]
        public void GetMemoryAsWordTest()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            var index = this._rand.Next(0, this._source.Memory.Length);
            var expected = new byte[2];
            expected[0] = this._source.Memory[index];
            expected[1] = this._source.Memory[index + 1];
            Array.Reverse(expected);
            var actual = this._source.GetMemoryAsWord(index, false);
            Assert.AreEqual(BitConverter.ToUInt16(expected, 0), actual, "Words do not match");
        }

        /// <summary>
        /// The get memory as word test 2.
        /// </summary>
        [Test]
        public void GetMemoryAsWordTest2()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            const string region = "Rom";
            var index = this._rand.Next(0, this._source.MemoryMap[region].Length - 1);
            var expected = new byte[2];
            expected[0] = this._source[region, index];
            expected[1] = this._source[region, index + 1];
            Array.Reverse(expected);
            var actual = this._source.GetMemoryAsWord(region, index, false);
            Assert.AreEqual(BitConverter.ToUInt16(expected, 0), actual, "Words do not match");
        }

        /// <summary>
        /// The get memory as double word test.
        /// </summary>
        [Test]
        public void GetMemoryAsDoubleWordTest()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            var index = this._rand.Next(0, this._source.Memory.Length);
            var expected = new byte[4];
            expected[0] = this._source.Memory[index];
            expected[1] = this._source.Memory[index + 1];
            expected[2] = this._source.Memory[index + 2];
            expected[3] = this._source.Memory[index + 3];
            Array.Reverse(expected);
            var actual = this._source.GetMemoryAsDoubleWord(index, false);
            Assert.AreEqual(BitConverter.ToUInt32(expected, 0), actual, "Words do not match");
        }

        /// <summary>
        /// The get memory as double word test 2.
        /// </summary>
        [Test]
        public void GetMemoryAsDoubleWordTest2()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            const string region = "Rom";
            var index = this._rand.Next(0, this._source.MemoryMap[region].Length);
            var expected = new byte[4];
            expected[0] = this._source[region, index];
            expected[1] = this._source[region, index + 1];
            expected[2] = this._source[region, index + 2];
            expected[3] = this._source[region, index + 3];
            Array.Reverse(expected);
            var actual = this._source.GetMemoryAsDoubleWord(region, index, false);
            Assert.AreEqual(BitConverter.ToUInt32(expected, 0), actual, "Words do not match");
        }

        /// <summary>
        /// The get memory as quad word test.
        /// </summary>
        [Test]
        public void GetMemoryAsQuadWordTest()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            var index = this._rand.Next(0, this._source.Memory.Length);
            var expected = new byte[8];
            expected[0] = this._source.Memory[index];
            expected[1] = this._source.Memory[index + 1];
            expected[2] = this._source.Memory[index + 2];
            expected[3] = this._source.Memory[index + 3];
            expected[4] = this._source.Memory[index + 4];
            expected[5] = this._source.Memory[index + 5];
            expected[6] = this._source.Memory[index + 6];
            expected[7] = this._source.Memory[index + 7];
            Array.Reverse(expected);
            var actual = this._source.GetMemoryAsQuadWord(index, false);
            Assert.AreEqual(BitConverter.ToUInt64(expected, 0), actual, "Words do not match");
        }

        /// <summary>
        /// The get memory as quad word test 2.
        /// </summary>
        [Test]
        public void GetMemoryAsQuadWordTest2()
        {
            this._source.ClearMemory();
            this.GenerateRandomData();
            const string region = "Rom";
            var index = this._rand.Next(0, this._source.MemoryMap[region].Length);
            var expected = new byte[8];
            expected[0] = this._source[region, index];
            expected[1] = this._source[region, index + 1];
            expected[2] = this._source[region, index + 2];
            expected[3] = this._source[region, index + 3];
            expected[4] = this._source[region, index + 4];
            expected[5] = this._source[region, index + 5];
            expected[6] = this._source[region, index + 6];
            expected[7] = this._source[region, index + 7];
            Array.Reverse(expected);
            var actual = this._source.GetMemoryAsQuadWord(region, index, false);
            Assert.AreEqual(BitConverter.ToUInt64(expected, 0), actual, "Words do not match");
        }

        /// <summary>
        ///     The NUnit test initialization method.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this._rand = new Random();
            this._source = new MemoryBase();
            this._source.MemoryMap.Add("Interpreter", new Range { Begin = 0, End = 511 });
            this._source.MemoryMap.Add("FontSet", new Range { Begin = 80, End = 160 });
            this._source.MemoryMap.Add("Rom", new Range { Begin = 512, End = 4095 });

            Assert.IsNotNull(this._source, "Memory Object not initialized");
        }

        /// <summary>
        ///     The memory test.
        /// </summary>
        [Test]
        public void MemoryTest()
        {
            const int expected = 4096;
            var actual = this._source.Memory.Length;
            Assert.AreEqual(expected, actual, "Memory size does not match.");
        }

        /// <summary>
        ///     The resize test.
        /// </summary>
        [Test]
        public void ResizeTest()
        {
            // Test Resize without preserving data.
            const int expected = 4096 * 2;
            this._source.Resize(expected);
            var actual = this._source.Memory.Length;
            Assert.AreEqual(expected, actual, "Memory size did not change.");

            // Test Resize while preserving data.
            this._source.ClearMemory();
            this.GenerateRandomData();

            // Do resize
            this._source.Resize(expected, true);
            actual = this._source.Memory.Length;
            Assert.AreEqual(expected, actual, "Memory size did not change.");

            // Compare with original
            var matched = this.CompareWithOriginal();

            Assert.IsTrue(matched, "Array did not preserve.");
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The compare with original.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        private bool CompareWithOriginal()
        {
            var matched = true;
            for (var i = 0; i < this._original.Length; i++)
            {
                matched &= this._original[i] == this._source.Memory[i];
            }

            return matched;
        }

        /// <summary>
        ///     The generate random data.
        /// </summary>
        private void GenerateRandomData()
        {
            // Fill memory with random data.
            for (var i = 0; i < this._source.Memory.Length; i++)
            {
                this._source.Memory[i] = Convert.ToByte(this._rand.Next(0, 256));
            }

            // Store original data.
            this._original = (byte[])this._source.Memory.Clone();
        }

        #endregion
    }
}