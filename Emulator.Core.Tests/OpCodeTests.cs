﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OpCodeTests.cs" company="TenneySoftware">
//   Copyright (c) 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Emulator.Core.Tests
{
    #region Usings

    using System;

    using NUnit.Framework;

    #endregion

    /// <summary>
    ///     The op code tests.
    /// </summary>
    [TestFixture]
    public class OpCodeTests
    {
        #region Fields

        /// <summary>
        ///     The _command action.
        /// </summary>
        private Action<ushort> _commandAction;

        /// <summary>
        ///     The _description.
        /// </summary>
        private string _description;

        /// <summary>
        ///     The _format.
        /// </summary>
        private string _format;

        /// <summary>
        ///     The _source.
        /// </summary>
        private OpCode _source;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The command action test.
        /// </summary>
        [Test]
        public void CommandActionTest()
        {
            Assert.AreEqual(this._commandAction, this._source.CommandActionParam, "Command Actions do not match.");
        }

        /// <summary>
        ///     The description test.
        /// </summary>
        [Test]
        public void DescriptionTest()
        {
            Assert.AreEqual(this._description, this._source.Description, "Descriptions do not match.");
        }

        /// <summary>
        ///     The format test.
        /// </summary>
        [Test]
        public void FormatTest()
        {
            Assert.AreEqual(this._format, this._source.Format, "Formats do not match.");
        }

        /// <summary>
        ///     The op code test.
        /// </summary>
        [SetUp]
        public void OpCodeTest()
        {
            this._format = "1NNN";
            this._description = "Clears the screen.";
            this._commandAction = (value) => Console.WriteLine("Cleared Screen");

            this._source = new OpCode(this._format, this._description, this._commandAction);
        }

        /// <summary>
        /// The value test.
        /// </summary>
        [Test]
        public void ValueTest()
        {
            const string expected = "1465";
            this._source.Value = expected;
            Assert.AreEqual(expected, this._source.Value, "Values do not match.");
        }

        #endregion
    }
}